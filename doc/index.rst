.. publib documentation master file, created by
   sphinx-quickstart on Thu Jul 20 13:29:56 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to publib's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 1

   pages/book
   pages/page
   pages/examples

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

