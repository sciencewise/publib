epub.Book
==================================

.. automodule:: epub

.. autoclass:: Book
    :inherited-members:
    :undoc-members:
    :members:
    :special-members: __init__, __getitem__, __delitem__, __iter__
