"""Module containing Book class."""
import re
import uuid
import json
import zipfile
from lxml import etree, html

import os.path as op

from .page import Page
from .utils import (get_container_xml, get_content_opf, get_toc,
                    build_concepts_page, build_bookmarks_page)
from .entities import Bookmark, Concept


class Book(object):
    """Type representing books in EPUB format processing."""

    def __init__(self, file=None, remove_empty_lines=True):
        """
        Book constructor.

        :param file: book file path
        :type file: str
        :param remove_empty_lines: should empty lines be removed
        :type remove_empty_lines: bool
        """
        self.info_fields = [
            'title', 'language', 'creator', 'date', 'description', 'publisher',
        ]
        self.ns = {
            'n': 'urn:oasis:names:tc:opendocument:xmlns:container',
            'pkg': 'http://www.idpf.org/2007/opf/',
            'dc': 'http://purl.org/dc/elements/1.1/',
            'atom': 'http://www.w3.org/2005/Atom',
            'ncx': 'http://www.daisy.org/z3986/2005/ncx/',
        }
        self.remove_empty_lines = remove_empty_lines
        if file:
            self.ifile = zipfile.ZipFile(file)  # Input file as a zip-archive.
            self.original = file  # Original file path.
            self.cfname = self._get_cfname()  # Content file name.
            self.book_info = self._get_book_info()
            self.book = self._get_book()
            self.concepts, self.bookmarks = self._get_additional_info()
        else:
            self.cfname = "content.opf"
            self.book_info = {}
            self.book = Page(None, "main", sub_pages=[])
            self.concepts, self.bookmarks = {}, {}

    @property
    def bookmarks_coords(self):
        """Return dict mapping bookmarks to list of page numbers where this bookmark is present."""
        bookmarks = {bm: [] for bm in self.bookmarks.keys()}
        for i, page in enumerate(self):
            for bm in page.bookmarks:
                bookmarks[bm].append(i)

        return bookmarks

    @property
    def concepts_coords(self):
        """Return dict mapping concepts to list of page numbers where this concept is present."""
        concepts = {c: [] for c in self.concepts.keys()}
        for i, page in enumerate(self):
            for c in page.concepts:
                concepts[c].append(i)

        return concepts

    def __getattr__(self, attr):
        """
        Return meta info from self.book_info.

        :return: meta info
        :rtype: str
        """
        return self.book_info[attr]

    def __getitem__(self, inds):
        """
        Get page by index.

        :param inds: index or indexes of page
        :type inds: int or tuple
        :return: page
        :rtype: Page
        """
        if isinstance(inds, int):
            obj = self.book.sub_pages[inds]
            return obj
        elif isinstance(inds, tuple):
            obj = self.book
            for ind in inds:
                if not isinstance(ind, int):
                    raise TypeError("Indexes must be integer type")
                obj = obj.sub_pages[ind]
            return obj

        raise TypeError("Indexes must be integer type")

    def __delitem__(self, inds):
        """
        Delete page by index.

        :param inds: index or indexes of page
        :type inds: int or tuple

        """
        if isinstance(inds, int):
            del self.book.sub_pages[inds]
        elif isinstance(inds, tuple):
            obj = self.book
            for ind in inds[:-1]:
                if not isinstance(ind, int):
                    raise TypeError("Indexes must be integer type")
                obj = obj.sub_pages[ind]
            del obj.sub_pages[inds[-1]]

    def __iter__(self):
        """
        Iterate over all pages and their subpages.

        :return: page
        :rtype: Page

        """
        def walk(ch):
            yield ch
            for p in ch.sub_pages:
                for elem in walk(p):
                    yield elem

        for ch in self.book.sub_pages:
            for p in walk(ch):
                yield p

    def _get_additional_info(self):
        """Parse and return book's custom concepts and bookmarks."""
        concepts = {}
        bookmarks = {}

        for filename in self.ifile.namelist():
            if "additional_info.json" in filename:
                data = json.loads(self.ifile.read(filename).decode("utf-8"))
                for key, value in data['bookmarks'].items():
                    bookmarks[key] = Bookmark(**value)
                for key, value in data['concepts'].items():
                    concepts[key] = Concept(**value)
                break  # Assuming there is only one file with such name.

        return concepts, bookmarks

    def _get_cfname(self):
        """Return content file name."""
        file = self.ifile.read('META-INF/container.xml')
        cfname = etree.fromstring(file).xpath('n:rootfiles/n:rootfile/@full-path', namespaces=self.ns)[0]

        return cfname

    def _get_book_info(self):
        """Return book information dictionary.

        E.g.: Creator, date, description, language, publisher and title fields.
        """
        epub_info = {}
        cftree = etree.fromstring(self.ifile.read(self.cfname))
        for s in self.info_fields:
            try:
                epub_info[s] = cftree.xpath('//dc:{0}/text()'.format(s), namespaces=self.ns)[0]
            except IndexError as e:
                epub_info[s] = ""

        return epub_info

    def _get_book(self):
        """Construct and return a tree-like structure representing the book by it's chapters."""
        chapters = []
        if len(self.cfname.split('/')) == 1:
            files_folder = None
        else:
            files_folder = self.cfname.split('/')[0]
        file_found = False
        for filename in self.ifile.namelist():
            if "toc.ncx" in filename:
                file_found = True
                tree = etree.fromstring(self.ifile.read(filename))
                sub_chapters = tree.xpath('ncx:navMap/ncx:navPoint', namespaces=self.ns)
                for sub_chapter in sub_chapters:
                    chap = self._parse_chapter(sub_chapter, files_folder)
                    chapters.append(chap)
                break  # Assuming there is only one file with such name.

        if not file_found:
            raise FileNotFoundError(".ncx not found")

        book = Page(None, "main", sub_pages=chapters)

        return book

    def _parse_chapter(self, chapter, files_folder):
        """Parse, construct chapter and return it as a `Book` object."""
        chapter_title = chapter.xpath(
            'ncx:navLabel/ncx:text/text()', namespaces=self.ns)[0]
        src = chapter.xpath('ncx:content/@src', namespaces=self.ns)[0]
        src = re.sub(r"#.+", "", src)

        pages = chapter.xpath('ncx:navPoint', namespaces=self.ns)

        sub_pages = []
        for p in pages:
            sub_pages.append(self._parse_chapter(p, files_folder))

        path = op.join(files_folder, src) if files_folder else src
        file_content = self.ifile.read(path)

        page = Page(
            html.fromstring(file_content),
            chapter_title,
            sub_pages=sub_pages,
            remove_empty_lines=self.remove_empty_lines)

        return page

    def _get_serializable_conc_bm(self):
        """Return book's concepts and bookmarks as a tuple of dictionaries."""
        concepts = {}
        bookmarks = {}

        for key, value in self.concepts.items():
            concepts[key] = value.to_dict()

        for key, value in self.bookmarks.items():
            bookmarks[key] = value.to_dict()

        return concepts, bookmarks

    def insert_bookmark(self, title, body):
        """
        Insert new bookmark.

        :param title: Bookmark's title text
        :param body: Bookmark's main content
        :type title: str
        :type body: str

        :return: uuid of created bookmark
        :rtype: str
        """
        id = str(uuid.uuid4())
        self.bookmarks[id] = Bookmark(title, body)

        return id

    def insert_concept(self, name, synonyms=None):
        """
        Insert new concept.

        :param name: Concept's name
        :param synonyms: Concept's set of synonyms
        :type name: str
        :type synonyms: set

        :return: uuid of created concept
        :rtype: str
        """
        if synonyms is None:
            synonyms = set()
        id = str(uuid.uuid4())
        self.concepts[id] = Concept(name, synonyms)

        return id

    def update_bookmark(self, uuid, title, body):
        """
        Update bookmark.

        :param uuid: Bookmarks's uuid
        :param title: Bookmarks's title text
        :param body: Bookmarks's body text
        :type uuid: str
        :type title: str
        :type body: set
        """
        bookmark = self.bookmarks[uuid]
        bookmark.title = title
        bookmark.body = body

    def update_concept(self, uuid, name, syns, replace_syns=False):
        """
        Update concept.

        :param uuid: Concept's uuid
        :param name: Concept's name
        :param syns: Concept's synonyms
        :param replace_syns: Replace synonyms or add new
        :type uuid: str
        :type name: str
        :type syns: set
        :type replace_syns: bool
        """
        syns = set(syns)
        concept = self.concepts[uuid]
        concept.name = name
        if replace_syns:
            concept.syns = syns
        else:
            concept.syns |= syns

    def delete_bookmark(self, uuid):
        """
        Delete bookmark.

        :param uuid: Bookmarks's uuid
        :type uuid: str
        """
        del self.bookmarks[uuid]

    def delete_concept(self, uuid):
        """
        Delete concept.

        :param uuid: Concept's uuid
        :type uuid: str
        """
        del self.concepts[uuid]

    def reduce_pages_count(self):
        """
        Join small pages to previous larger page.

        :return: self
        :rtype: Book
        """
        def reducer(root_page):
            root_page.merge_small_pages()
            for page in root_page.sub_pages:
                reducer(page)

        reducer(self.book)

        return self

    def flatten_all(self):
        """
        Flat book tree structure into tree with with depth = 1.

        :return: self
        :rtype: Book
        """
        pages = []
        for sub_page in self.book.sub_pages:
            pages.extend(sub_page.merge())

        self.book.sub_pages = pages

        return self

    def commit(self, filename, output_type=None):
        """
        Commit book into epub file.

        :param filename: output file
        :param output_type: type of output file ('epub' or 'vico')
        :type filename: str
        :type output_type: str or None
        """
        if not (output_type in {"epub", "vico", None}):
            raise ValueError("'output_type' must be 'epub', 'vico' or None")

        zfile = zipfile.ZipFile(filename, mode="w")

        zfile.writestr("mimetype", "application/epub+zip")

        files_folder = self.cfname.split("/")[0]

        concepts, bookmarks = self._get_serializable_conc_bm()

        additional_data = json.dumps({
            "bookmarks": bookmarks,
            "concepts": concepts,
        })

        zfile.writestr(op.join(files_folder, "additional_info.json"), additional_data)
        meta = etree.tostring(
            get_container_xml(files_folder),
            pretty_print=True,
            xml_declaration=True,
            encoding='UTF-8')
        zfile.writestr("META-INF/container.xml", meta)

        content_opf = etree.tostring(
            get_content_opf(self),
            pretty_print=True,
            xml_declaration=True,
            encoding='UTF-8')
        zfile.writestr(op.join(files_folder, "content.opf"), content_opf)

        toc = etree.tostring(
            get_toc(self),
            pretty_print=True,
            xml_declaration=True,
            encoding='UTF-8')
        zfile.writestr(op.join(files_folder, "toc.ncx"), toc)

        for i, page in enumerate(self.__iter__()):
            page = etree.tostring(
                page.html,
                pretty_print=True,
                xml_declaration=True,
                encoding='UTF-8')
            zfile.writestr(op.join(files_folder, "page{0}.xhtml".format(i + 1)), page)

        if output_type == 'epub':
            page = etree.tostring(
                build_concepts_page(self.concepts),
                pretty_print=True,
                xml_declaration=True,
                encoding='UTF-8')
            zfile.writestr(op.join(files_folder, "concepts.xhtml"), page)
            page = etree.tostring(
                build_bookmarks_page(self.bookmarks),
                pretty_print=True,
                xml_declaration=True,
                encoding='UTF-8')
            zfile.writestr(op.join(files_folder, "bookmarks.xhtml"), page)

        zfile.close()
