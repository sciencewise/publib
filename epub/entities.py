"""This module contains different EPUB entities classes."""
import re
from copy import deepcopy
from collections import namedtuple
from lxml import html, etree

Reference = namedtuple("Reference", "start finish")

reference_re = re.compile(r"\[\d+\]")


class Concept(object):
    """Concept class."""

    def __init__(self, name, synonyms=None):
        """Concept's constructor."""
        if synonyms is None:
            synonyms = set()
        self.name = name
        self.synonyms = synonyms

    def __str__(self):
        """Return object's representation."""
        return "Concept(name={0}, synonyms={1})".format(
            self.name, str(self.synonyms))

    def __repr__(self):
        """Return object's representation."""
        return self.__str__()

    def to_dict(self):
        """Convert to dictionary."""
        return {"name": self.name, "synonyms": list(self.synonyms)}


class Bookmark(object):
    """Bookmark class."""

    def __init__(self, title, body):
        """Bookmark's constructor."""
        self.title = title
        self.body = body

    def __str__(self):
        """Return object's representation."""
        return "Bookmark(title={0}, body={1}...)".format(
            self.title, self.body[:32])

    def __repr__(self):
        """Return object's representation."""
        return self.__str__()

    def to_dict(self):
        """Convert to dictionary."""
        return {"title": self.title, "body": self.body}


class SimpleParagraph(object):
    """SimpleParagraph class."""

    def __init__(self, par, tag, **attrs):
        """TODO: Add docstring."""
        self.html = deepcopy(par)
        self.html.tail = None
        self.html.tag = tag
        for key, val in attrs.items():
            self.html.set(key, val)
        self.text = self.html.text_content().strip()

    def __str__(self):
        """Return object's representations."""
        return "SimpleParagraph(text={0})".format(self.text)


class Poem(object):
    """TODO: Add docstring."""

    def __init__(self, par):
        """TODO: Add docstring."""
        stanzas = []
        text = []
        for stanza in par.cssselect(".stanza"):
            stanza_block = []
            for p in stanza.cssselect("p"):
                stanza_block.append(SimpleParagraph(p,
                                                    tag="p",
                                                    align="center"),
                                    )
                text.append(stanza_block[-1].text)
            stanzas.append(stanza_block)

        self.text = ' '.join(text)
        self.html = self.__build_html(stanzas)

    def __build_html(self, poem):
        root = html.Element("div")
        root.set("class", "poem")

        for stanza in poem:
            stanza_tag = etree.SubElement(root, "div")
            stanza_tag.set("class", "stanza")
            for line in stanza:
                stanza_tag.append(line.html)

        return root


class Epigraph(object):
    """TODO: Add docstring."""

    def __init__(self, par):
        """TODO: Add docstring."""
        epigraph = []
        text = []
        for p in par.cssselect("p"):
            epigraph.append(SimpleParagraph(p,
                                            tag=get_tag(p),  # NOQA TODO: Where is `get_tag` defined?
                                            align="right"),
                            )
            text.append(epigraph[-1].text)

        self.text = ' '.join(text)
        self.html = self.__build_html(epigraph)

    def __build_html(self, epigraph):
        root = html.Element("div")
        root.set("class", "epigraph")

        for line in self.epigraph:
            root.append(line.html)

        return root
