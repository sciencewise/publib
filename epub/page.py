"""Module containing Page class."""
import copy
import itertools
from lxml import etree

from .utils import get_paragraph


class Page(object):
    """
    Type representing pages in EPUB format processing.

    And also chapters and some yet-to-be-discovered sub-pages.
    """

    def __init__(self, tree, name, sub_pages=None, remove_empty_lines=False):
        """
        Page constructor.

        :param tree: xml page in epub book
        :type tree: lxml.etree.Element or None
        :param name: name of page
        :type name: str
        :param sub_pages: list of subpages
        :type sub_pages: list of `Page`
        :param remove_empty_lines: condtion to remove empty paragraphs
        :type remove_empty_lines: bool
        """
        if sub_pages is None:
            sub_pages = list()
        self.tree = tree
        self.name = name
        self.sub_pages = sub_pages
        self.remove_empty_lines = remove_empty_lines
        self.paragraphs = [] if self.tree is None else self._get_content(tree)

    @property
    def bookmarks(self):
        """Return list of bookmarks."""
        return [
            tag.get("bookmarks").split()
            for tag in self.tree.xpath("//span[@bookmarks]")
        ]

    @property
    def concepts(self):
        """Return list of concepts."""
        return [
            tag.get("concepts").split()
            for tag in self.tree.xpath("//span[@concepts]")
        ]

    def __iadd__(self, page2):
        """
        Add subpages and paragraphs from another page. Used in page merging.

        :param page2: another page
        :type page2: Page
        :return: self
        :rtype: Page
        """
        self.sub_pages += page2.sub_pages
        self.paragraphs += page2.paragraphs

        return self

    def __getitem__(self, par_id):
        """
        Return paragraph by following position.

        :param par_id: Paragraph position in page
        :type par_id: int
        :return: Paragraph
        :rtype: SimpleParagraph or Poem or Epigraph
        """
        return self.paragraphs[par_id]

    def __delitem__(self, par_id):
        """
        Remove paragraph by following position.

        :param par_id: Paragraph position in page
        :type par_id: int
        """
        del self.paragraphs[par_id]

    def __len__(self):
        """Return paragraphs count."""
        return len(self.paragraphs)

    def __iter__(self):
        """
        Iterate over all paragraphs in page.

        :return: Paragraph
        :rtype: SimpleParagraph or Poem or Epigraph
        """
        for par in self.paragraphs:
            yield par

    def __str__(self):
        """Return object's representation."""
        return 'Page(name="{0}")'.format(self.name)

    def __repr__(self):
        """Return object's representation."""
        return self.__str__()

    def _get_content(self, tree):
        paragraphs = []
        for p in self._get_body_content(tree):
            p = get_paragraph(p)
            if not self.remove_empty_lines or p.text.strip():
                paragraphs.append(p)
        return paragraphs

    def _get_body_content(self, tree):
        etree.strip_tags(tree, 'a', 'b', 'i', 'em')
        elements = tree.body

        if (len(elements) == 1 and elements[0].tag == "div" and
                "class" in elements[0].attrib and
                "section" in elements[0].attrib["class"]):
            elements = elements[0]

        for element in elements:
            attrs = element.attrib
            if element.tag in {"p"} | {"h{0}".format(i) for i in range(1, 7)}:
                yield element
            elif "class" in attrs:
                if ("title" in attrs["class"] or
                        "subtitle" in attrs["class"] or
                        "poem" in attrs["class"]):
                    yield element

    def update_tree(self, new_tree):
        """
        Update page tree and paragraphs.

        :param new_tree: xml page in epub book
        :type new_tree: lxml.etree.Element
        """
        self.tree = new_tree
        self.paragraphs = self._get_content(new_tree)

    def merge(self):
        """
        Recursive merge all subpages and their content.

        :return: Subpages tree with depth = 1
        :rtype: list of Page
        """
        if self.sub_pages:
            sub_pages_new = itertools.chain.from_iterable(p.merge() for p in self.sub_pages)
            sub_pages_new = list(sub_pages_new)
            sub_pages_new[0].paragraphs = self.paragraphs + sub_pages_new[0].paragraphs
            sub_pages_new[0].name = '. '.join([self.name, sub_pages_new[0].name])

            return sub_pages_new

        return [copy.deepcopy(self)]

    def merge_small_pages(self):
        """
        Merge pages with small content size to previous page.

        :return: self
        :rtype: Page
        """
        if self.sub_pages and all(len(page.sub_pages) == 0 for page in self.sub_pages):
            new_sub_pages = [self.sub_pages[0]]

            for page in self.sub_pages[1:]:
                if sum(len(par.text.split()) for par in new_sub_pages[-1]) < 32:
                    new_sub_pages[-1] += page
                else:
                    new_sub_pages.append(page)

            self.sub_pages = new_sub_pages

        return self

    @property
    def html(self):
        """
        Return html representation of this page.

        :return: Html page
        :rtype: lxml.etree.Element
        """
        root = etree.Element("html", nsmap={None: "http://www.w3.org/1999/xhtml"})
        head = etree.SubElement(root, "head")
        title = etree.SubElement(head, "title")
        title.text = self.name
        body = etree.SubElement(root, "body")

        for par in self.paragraphs:
            body.append(par.html)

        return root

    def pprint(self):  # Because `print` is a reserved word in Python2.
        """Print page's content to stdout."""
        print(etree.tostring(self.html, pretty_print=True, encoding='unicode'))
