"""This module contains a few random helpful functions."""
import itertools
from os import path
from lxml import etree

from .entities import Poem, Epigraph, SimpleParagraph


def get_paragraph(par):
    """Construct and return an object of correct paragraph type."""
    attrs = par.attrib
    if 'class' in attrs:
        if 'epigraph' in attrs['class']:
            return Epigraph(par)
        elif 'poem' in attrs['class']:
            return Poem(par)
        elif 'title' in attrs['class']:
            return SimpleParagraph(par[0], "h2")
    return SimpleParagraph(par, par.tag)


def get_container_xml(content_path):
    """Construct and return an XML object describing location of content file."""
    root = etree.Element("container", version="1.0",
                         nsmap={None: "urn:oasis:names:tc:opendocument:xmlns:container"})
    root_files = etree.SubElement(root, "rootfiles")
    root_file = etree.SubElement(root_files, "rootfile")
    root_file.set("full-path", path.join(content_path, "content.opf"))
    root_file.set("media-type", "application/oebps-package+xml")

    return root


def get_content_opf(book):
    """TODO: add a description."""
    root = etree.Element("package", version="2.0",
                         nsmap={None: "http://www.idpf.org/2007/opf"})

    metadata = etree.SubElement(root, "{http://www.idpf.org/2007/opf}metadata",
                                nsmap={
                                    "dc": "http://purl.org/dc/elements/1.1/",
                                    "opf": "http://www.idpf.org/2007/opf",
                                    },
                                )
    for key, val in book.book_info.items():
        met = etree.SubElement(metadata, "{{http://purl.org/dc/elements/1.1/}}{0}".format(key))
        met.text = val

    manifest = etree.SubElement(root, "manifest")
    manifest_toc = etree.SubElement(manifest, "item")
    manifest_toc.set("id", "ncx")
    manifest_toc.set("media-type", "application/x-dtbncx+xml")
    manifest_toc.set("href", "toc.ncx")

    adds = etree.SubElement(manifest, "item")
    adds.set("id", "addinfo")
    adds.set("media-type", "application/json")
    adds.set("href", "additional_info.json")

    spine = etree.SubElement(root, "spine")
    spine.set("toc", "ncx")

    for i, _page in enumerate(book):
        manifest_page = etree.SubElement(manifest, "item")
        manifest_page.set("href", "page{0}.xhtml".format(i+1))
        manifest_page.set("id", "id{0}".format(i+1))
        manifest_page.set("media-type", "application/xhtml+xml")

        spine_element = etree.SubElement(spine, "itemref")
        spine_element.set("idref", "id{0}".format(i+1))

    return root


def get_toc(book):
    """Build and return a Table Of Contents xml page."""
    root = etree.Element("ncx", version="2005-1",
                         nsmap={None: "http://www.daisy.org/z3986/2005/ncx/"})
    etree.SubElement(root, "head")
    title = etree.SubElement(root, "docTitle")
    text_title = etree.SubElement(title, "text")
    text_title.text = book.title
    navMap = etree.SubElement(root, "navMap")

    def recursive_books_filler(sub_pages, root, gen):
        for sub_page in sub_pages:
            num = next(gen)
            child = etree.SubElement(root, "navPoint",
                                     id="NavPoint-{0}".format(num), playOrder=str(num))
            label = etree.SubElement(child, "navLabel")
            text = etree.SubElement(label, "text")
            text.text = sub_page.name
            child.append(etree.Element("content", src="page{0}.xhtml".format(num)))

            recursive_books_filler(sub_page.sub_pages, child, gen)

    recursive_books_filler(book.book.sub_pages, navMap, itertools.count(1))

    return root


def build_concepts_page(concepts):
    """Build and return a `lxml.etree._Element` object representing page with concepts."""
    root = etree.Element("html",
                         nsmap={None: "http://www.w3.org/1999/xhtml"})
    head = etree.SubElement(root, "head")
    title = etree.SubElement(head, "title")
    title.text = "Concepts"
    body = etree.SubElement(root, "body")

    for key, value in concepts.items():
        ref = etree.SubElement(body, "a")
        ref.set("id", key)
        name_content = etree.SubElement(body, "h4")
        name_content.text = value.name
        list_content = etree.SubElement(body, "ul")
        for syn in value.synonyms:
            list_elem = etree.SubElement(list_content, "li")
            list_elem.text = syn

    return root


def build_bookmarks_page(bookmarks):
    """Build and return a `lxml.etree._Element` object representing page with bookmarks."""
    root = etree.Element("html",
                         nsmap={None: "http://www.w3.org/1999/xhtml"})
    head = etree.SubElement(root, "head")
    title = etree.SubElement(head, "title")
    title.text = "Bookmarks"
    body = etree.SubElement(root, "body")

    for key, value in bookmarks.items():
        ref = etree.SubElement(body, "a")
        ref.set("id", key)
        title_content = etree.SubElement(body, "h4")
        title_content.text = value.title
        body_content = etree.SubElement(body, "p")
        body_content.text = value.body

    return root


def print_html(html):
    """Prettyprint `lxml.html.HtmlElement` object."""
    print(etree.tostring(html, pretty_print=True, encoding='unicode'))
