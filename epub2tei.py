"""TODO: Add docstring."""
import argparse
import lxml.etree as ET

import epub


parser = argparse.ArgumentParser(description='Process epub book')
parser.add_argument('book', type=str, help='Path to epub book')
parser.add_argument('--out', type=str, help='Path to output file', default="book.xml")
parser.add_argument('--title', type=str, help='Book\'s title', default="")
parser.add_argument('--author', type=str, help='Book\'s author', default="")


def main(args):
    """TODO: Add docstring."""
    book = ET.Element('TEI')
    xml_result = ET.ElementTree(book)

    teiHeader = ET.SubElement(book, 'teiHeader')
    fileDesc = ET.SubElement(teiHeader, 'fileDesc')
    titleStmt = ET.SubElement(fileDesc, 'titleStmt')

    title = ET.SubElement(titleStmt, 'title')
    title.text = args.title
    author = ET.SubElement(titleStmt, 'author')
    author.text = args.author

    respStmt = ET.SubElement(titleStmt, 'respStmt')
    ET.SubElement(respStmt, 'resp')
    ET.SubElement(respStmt, 'name')
    text_data = ET.SubElement(book, 'text')

    b = epub.Book(args.book).flatten_all().reduce_pages_count()

    pages_counter = 1

    for page in b:
        for par in page:
            text_data.append(par.html)
        if page.paragraphs:
            ET.SubElement(text_data, "pb", n=str(pages_counter))
            pages_counter += 1

    with open(args.out, "wb") as f:
        xml_result.write(f, encoding='UTF-8', xml_declaration=True, pretty_print=True)


if __name__ == '__main__':
    args = parser.parse_args()
    main(args)
