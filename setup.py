"""Setup file."""
from __future__ import print_function
from setuptools import setup, find_packages


install_requires = ['lxml']

setup(
    name='publib',
    version='0.1',
    description='Library for epub processing',
    author='Artem Sukhodolsky',
    author_email='vangogius@gmail.com',
    url='https://bitbucket.org/sciencewise/publib',
    classifiers=[
         "Programming Language :: Python :: 2.7",
         "Programming Language :: Python :: 3.4",
         "Programming Language :: Python :: 3.5",
         "Programming Language :: Python :: 3.6",
    ],

    include_package_data=True,
    packages=find_packages('.', exclude=["tests"]),
    install_requires=install_requires,
)
