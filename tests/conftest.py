import codecs
import pytest
from lxml import html

from . import Book


@pytest.fixture
def book():
    return Book("tests/data/test.epub")


@pytest.fixture
def book_mm():
    return Book("tests/data/mm.epub")


@pytest.fixture
def flat_book_mm():
    return Book("tests/data/mm.epub").flatten_all()


@pytest.fixture
def flat_book():
    return Book("tests/data/test.epub").flatten_all()


@pytest.fixture
def book_reduced():
    return Book("tests/data/mm.epub").flatten_all().reduce_pages_count()


@pytest.fixture
def sample_page():
    with codecs.open("tests/data/new_page.xhtml", "r", "utf-8") as f:
        tree = html.fromstring(f.read().encode("utf-8"))
    return tree
