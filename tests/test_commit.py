# *=* coding=utf-8 *=*

from lxml import html
from epub import Book


par_string = html.fromstring(
    u"<p>На тот – <span bookmarks='1 2 3' concepts='1 2 3'>легко допустимый</span> – случай, если Вы обо мне совсем ничего не вспомните, представлюсь еще раз: меня зовут Франц Кафка, я тот самый человек, который впервые имел возможность поздороваться с Вами в Праге в доме господина директора Брода и который затем весь вечер протягивал Вам через стол одну за одной фотографии талийского путешествия, а в конце концов вот этой же рукой, которая сейчас выстукивает по клавишам, сжимал Вашу ладонь, коим рукопожатием было скреплено Ваше намерение и даже обещание на следующий год совершить вместе с ним путешествие в Палестину.</p>".  # NOQA
    strip())


def test_writing(book):
    book.insert_concept("concept", {"c1", "c2"})
    book.insert_bookmark("bookmark", "aaweae")
    book.commit("tests/data/saved.epub")

    restored = Book("tests/data/saved.epub")
    assert book.title == restored.title
    assert book.creator == restored.creator
    assert book.publisher == restored.publisher
    assert book.language == restored.language
    assert book.date == restored.date
    assert len(list(book)) == len(list(restored))

    assert list(restored.bookmarks.values())[0].title == 'bookmark'
    assert list(restored.concepts.values())[0].name == 'concept'


def test_writing_epub_compatibility(book, sample_page):
    book.insert_concept("concept", {"c1", "c2"})
    book.insert_bookmark("bookmark", "aaweae")
    book[0].update_tree(sample_page)

    book.commit("tests/data/saved_epub.epub", "epub")

    restored = Book("tests/data/saved_epub.epub")
    assert list(restored.bookmarks.values())[0].title == 'bookmark'
    assert list(restored.concepts.values())[0].name == 'concept'

    elements_equal(restored[0][2].html, par_string)


def test_writing_vico_compatibility(book, sample_page):
    book.insert_concept("concept", {"c1", "c2"})
    book.insert_bookmark("bookmark", "aaweae")
    book[0].update_tree(sample_page)

    book.commit("tests/data/saved_vico.epub", "vico")

    restored = Book("tests/data/saved_vico.epub")
    assert list(restored.bookmarks.values())[0].title == 'bookmark'
    assert list(restored.concepts.values())[0].name == 'concept'

    elements_equal(restored[0][2].html, par_string)


def elements_equal(e1, e2):
    assert e1.tag == e2.tag
    assert e1.text == e2.text
    assert e1.tail == e2.tail
    assert e1.attrib == e2.attrib
    assert len(e1) == len(e2)
