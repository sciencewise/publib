# *=* coding=utf-8 *=*

import pytest


def test_indexing(book):
    assert book[0].name == u'1912'
    assert book[0, 0].name == u'Сентябрь'
    assert book[0, 0, 0].name == u'20.09.1912'
    assert book[0, 1, -1].name == u'31.10.1912'


def test_page_removing(book):
    assert book[0].name == u'1912'
    del book[0]
    assert book[0].name == u'1913'


def test_paragraph_removing(flat_book):
    assert flat_book[0][0].text == u'1912'
    del flat_book[0][0]
    assert flat_book[0][0].text == u'Сентябрь'


def test_indexing_mm(book_mm):
    assert book_mm[0].name == u'Page titre'
    assert book_mm[1].name == u'PREMIÈRE PARTIE'


def test_exceptions(book):
    with pytest.raises(IndexError):
        book[0, 0, 0, 0]
        book[1000]

    with pytest.raises(TypeError):
        book['aew', 'a']
        book['aew']


def test_exceptions_mm(book_mm):
    with pytest.raises(IndexError):
        book_mm[0, 0, 0, 0]
        book_mm[1000]

    with pytest.raises(TypeError):
        book_mm['aew', 'a']
        book_mm['aew']


def test_flattening(flat_book):
    assert flat_book[0].name == u'1912. Сентябрь. 20.09.1912'
    assert flat_book[1].name == u'28.09.1912'
    assert flat_book[2].name == u'Октябрь. 13.10.1912'


def test_exceptions_flat(flat_book):
    with pytest.raises(IndexError):
        flat_book[0, 0]
