# *=* coding=utf-8 *=*


def test_book_info(book):
    assert book.title == u'Письма к Фелиции'
    assert book.creator == u'Франц  Кафка'
    assert book.publisher == u'Азбука-классика'
    assert book.language == u'ru'
    assert book.date == u''
    assert len(list(book)) == 342


def test_book_info_mm(book_mm):
    assert book_mm.title == u'Le Maître et Marguerite'
    assert book_mm.creator == u'Mikhaïl Boulgakov'
    assert book_mm.publisher == u'Ebooks libres et gratuits'
    assert book_mm.language == u'fr'
    assert book_mm.date == u'2009-11-18'
    assert len(list(book_mm)) == 37
