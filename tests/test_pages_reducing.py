# *=* coding=utf-8 *=*


def test_page_reducing(book_reduced):
    assert len(list(book_reduced)) == 34
    assert book_reduced[0][3].text == u'GOETHE, Faust.'
    assert book_reduced[1][0].text == u'CHAPITRE II – Ponce Pilate'
