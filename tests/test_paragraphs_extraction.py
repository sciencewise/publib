# *=* coding=utf-8 *=*


def test_paragraph_extraction1(flat_book_mm):
    assert flat_book_mm[1][0].text == u"PREMIÈRE PARTIE"
    assert flat_book_mm[2][0].text == u"CHAPITRE II – Ponce Pilate"


def test_paragraph_extraction2(book):
    assert book[1][0].text == u"1913"
    assert book[1, 0][0].text == u"Январь"
